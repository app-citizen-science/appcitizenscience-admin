import {Icon} from "leaflet";

// Marker customization
export const markerIconStart = new Icon({
    iconUrl: "/sod_start.svg",
    iconSize: [50, 50],
});

export const markerIconEnd = new Icon({
    iconUrl: "/sod_end.svg",
    iconSize: [50, 50],
});