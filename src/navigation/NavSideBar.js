import React, {useContext, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import {NavLink, withRouter} from 'react-router-dom';
import {AdminRoutes, ExpertRoutes} from './Routes';

import {AppBar, Drawer, IconButton, ListItemText, MenuItem, MenuList, Toolbar, Typography,} from '@material-ui/core';
import {HeaderComponent, LogoutComponent, NavBarName, RoleNameManagement} from "../utils/NameManagement";
import {UserContext} from "../UserContext";
import Divider from "@material-ui/core/Divider";


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    drawer: {
        width: 300,
    },
    userInfo: {
        marginBottom: -10,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    }
}));



//DISPLAY THE NAVBAR ACCORDING TO THE ROLE WITH THEIR OWN ROUTES ADMIN-EXPERT


const NavSideBar = () => {
    const classes = useStyles();
    const [drawerOpened, setDrawerOpened] = useState(false);
    const toggleDrawer = booleanValue => () => {
        setDrawerOpened(booleanValue)
    }

    const activeRoute = (routeName, pathName) => {
        return pathName === routeName;
    }

    const LinkMenuList = withRouter(props => {
        const {location} = props;


        return (
            <>
                {UserType.user.role === 'Administrator' ?

                    <MenuList>
                        {AdminRoutes.map((prop) => {
                            return (
                                <NavLink to={prop.path} style={{textDecoration: 'none'}}>
                                    <MenuItem selected={activeRoute(prop.path, location.pathname)}>
                                        <ListItemText primary={NavBarName(prop.sidebarName)}/>
                                    </MenuItem>
                                </NavLink>
                            );
                        })}
                    </MenuList>

                    :

                    <MenuList>
                        {ExpertRoutes.map((prop) => {
                            return (
                                <NavLink to={prop.path} style={{textDecoration: 'none'}}>
                                    <MenuItem selected={activeRoute(prop.path, location.pathname)}>
                                        <ListItemText primary={NavBarName(prop.sidebarName)}/>
                                    </MenuItem>
                                </NavLink>
                            );
                        })}
                    </MenuList>


                }
            </>

        );
    });

    let UserType = useContext(UserContext);

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu"
                                onClick={toggleDrawer(true)}>
                        <MenuIcon/>
                    </IconButton>
                    <Typography variant="h6" className={classes.title}>



                        <HeaderComponent/>



                    </Typography>

                    <LogoutComponent/>

                </Toolbar>
            </AppBar>
            <Drawer
                classes={{paper: classes.drawer}}
                anchor="left"
                open={drawerOpened}
                onClose={toggleDrawer(false)}
            >
                <div
                    onClick={toggleDrawer(false)}
                    onKeyDown={toggleDrawer(false)}
                >
                    <h2 className={classes.userInfo}>
                        {
                            RoleNameManagement(UserType.user.role)
                        }
                    </h2>
                    <h4 className={classes.userInfo} >{UserType.user.username}</h4>
                    <h4 className={classes.userInfo} style={{marginBottom: 20}}>{UserType.user.email}</h4>
                    <Divider />
                    <LinkMenuList/>
                </div>
            </Drawer>
        </div>
    );
}

export default withRouter(NavSideBar);