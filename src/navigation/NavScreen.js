import React, {useContext} from 'react';
import "../App.css";
import {Route, Switch} from 'react-router-dom';
import {AdminRoutes, ExpertRoutes} from './Routes';
import NavSideBar from './NavSideBar';
import {UserContext} from "../UserContext";


//NAVIGATION CALLING THE ROUTES ACCORDING TO THE ROLE OF THE USER USING CONTEXT

export function NavScreen() {

    let UserType = useContext(UserContext);

    return (

        <>
            {
                UserType.user === undefined
                ? null
                    : UserType.user.role === 'Administrator' ?

                    <div>
                        <NavSideBar/>
                        <Switch>
                            {AdminRoutes.map((route) => (
                                <Route exact path={route.path} key={route.path}>
                                    <route.component/>

                                </Route>
                            ))}
                        </Switch>
                    </div>
                    :
                    <div>
                        <NavSideBar/>
                        <Switch>
                            {ExpertRoutes.map((route) => (
                                <Route exact path={route.path} key={route.path}>
                                    <route.component/>
                                </Route>
                            ))}
                        </Switch>
                    </div>
            }
        </>
    );
}
