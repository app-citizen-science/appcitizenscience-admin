import Contact from '../screens/Contact';
import UserManagement from "../screens/UserManagement";
import Home from "../screens/Home";
import SodPodValidation from "../screens/SodPodValidation";
import AllSodPodValidation from "../screens/AllSodPodValidation";


//ROUTES FOR ADMIN AND EXPERT


export const AdminRoutes = [

  {
    path: '/',
    sidebarName:  "Home",
    component: Home
  },
  {
    path: '/user-management',
    sidebarName:  "User management",
    component: UserManagement
  },
  {
    path: '/contact',
    sidebarName: "Contact",
    component: Contact
  },


];




export const ExpertRoutes = [

  {
    path: '/',
    sidebarName:  "Home",
    component: Home
  },
  {
    path: '/pod-sod-validation',
    sidebarName:  "POD & SOD Validation",
    component: SodPodValidation
  },
  {
    path: '/all-pod-sod-validation',
    sidebarName:  "All POD & SOD Validation",
    component: AllSodPodValidation
  },
  {
    path: '/contact',
    sidebarName: "Contact",
    component: Contact
  },

];
