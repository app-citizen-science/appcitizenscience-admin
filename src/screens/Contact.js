import {Grid, makeStyles, Paper} from "@material-ui/core";
import React from "react";
import {useTranslation} from "react-i18next";


//Example contact page

export default function Contact () {

  const [t] = useTranslation('common');
  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1
    },
    paper: {
      padding: theme.spacing(1),
      margin: 15,
      textAlign: 'center',
      color: theme.palette.text.primary,
      borderRadius: "25px",
      backgroundPosition: "left top",
      backgroundRepeat: "repeat",
    },
  }))
  const classes = useStyles();


  return(


      <div className={classes.root}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
            <h1>{t("_contact")}</h1>
            </Paper>
          </Grid>
          <Grid item xs={6}>
            <Paper className={classes.paper}>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc dolor massa, cursus dictum urna vel, varius iaculis urna. Nam ac elit mollis, bibendum libero et, interdum dolor. Curabitur molestie, velit in euismod viverra, felis mauris tincidunt est, sit amet suscipit mauris ex id purus. Mauris quam urna, lobortis finibus ligula vel, maximus viverra nisl. Sed molestie elit augue, at suscipit elit commodo id. Duis volutpat, nibh sed bibendum accumsan, arcu augue eleifend justo, quis sagittis mauris nisi a enim. Integer ut lorem vehicula, commodo nisl euismod, ultricies nunc. Suspendisse rhoncus, lectus nec iaculis mollis, metus massa rutrum mauris, nec malesuada turpis tellus a ex. Morbi eget tempus nulla. Sed eget placerat augue. </p>
            </Paper>
          </Grid>

          <Grid item xs={6}>
            <Paper className={classes.paper}>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc dolor massa, cursus dictum urna vel, varius iaculis urna. Nam ac elit mollis, bibendum libero et, interdum dolor. Curabitur molestie, velit in euismod viverra, felis mauris tincidunt est, sit amet suscipit mauris ex id purus. Mauris quam urna, lobortis finibus ligula vel, maximus viverra nisl. Sed molestie elit augue, at suscipit elit commodo id. Duis volutpat, nibh sed bibendum accumsan, arcu augue eleifend justo, quis sagittis mauris nisi a enim. Integer ut lorem vehicula, commodo nisl euismod, ultricies nunc. Suspendisse rhoncus, lectus nec iaculis mollis, metus massa rutrum mauris, nec malesuada turpis tellus a ex. Morbi eget tempus nulla. Sed eget placerat augue. </p>
            </Paper>
          </Grid>

          </Grid>
      </div>


    );
}