import React, {useEffect, useState} from "react";
import {useTranslation} from "react-i18next";
import {useHistory} from "react-router-dom";
import {myUserPodsStream, myUserSodsStream} from "../database/firestore_requests";
import {mapDocToPod, mapDocToSod} from "../database/mapping_methods";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import IconButton from "@material-ui/core/IconButton";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import Collapse from "@material-ui/core/Collapse";
import Box from "@material-ui/core/Box";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import {StatusIconManagement} from "../utils/NameManagement";


//CALLED BY USERS POD SOD TABLE, THE ROW IS FILLED WITH THE INFORMATIONS ABOUT POD AND SOD ACCORDING TO THE USER

export default function Row(props) {
    const {row} = props;
    const [open, setOpen] = React.useState(false);
    const [t] = useTranslation('common');
    let [userPods, setUserPods] = useState([]);
    let [userSods, setUserSods] = useState([]);
    let history = useHistory();
    let userId = props.row.user_id;

    const handleNavigationSod = (id) => {
        history.push(
            {
                pathname: `/sod/${id}`,
                state: {username: row.username}
            }
        )
    }

    const handleNavigationPod = (id) => {
        history.push(
            {
                pathname: `/pod/${id}`,
                state: {username: row.username}
            }
        )
    }


    useEffect(() => {

        //Get a list of the pods created by the user connected from firestore and map every document to pod objects
        //Subscribe to any changes of the data and unsubscribe when not necessary anymore
        const unsubscribe = myUserPodsStream({
            next: querySnapshot => {
                const userPodsFirestore = querySnapshot.docs
                    .map((docSnapshot, index) => {
                        if (docSnapshot.data().pod_id !== undefined) {
                            return mapDocToPod(docSnapshot);
                        }
                    });


                userPodsFirestore.sort((a, b) => {
                    return a.date - b.date
                });


                setUserPods(userPodsFirestore);
            },
            error: (error) => {
                setUserPods([]);
            }
        }, userId);
        return () => unsubscribe();


    }, [setUserPods]);


    useEffect(() => {

        //Get a list of the pods created by the user connected from firestore and map every document to pod objects
        //Subscribe to any changes of the data and unsubscribe when not necessary anymore
        const unsubscribe = myUserSodsStream({
            next: querySnapshot => {
                const userSodsFirestore = querySnapshot.docs
                    .map((docSnapshot, index) => {
                        if (docSnapshot.data().sod_id !== undefined) {
                            return mapDocToSod(docSnapshot);
                        }
                    });

                userSodsFirestore.sort((a, b) => {
                    return a.date - b.date
                });


                setUserSods(userSodsFirestore);
            },
            error: (error) => {
                setUserSods([]);
            }
        }, userId);
        return () => unsubscribe();


    }, [setUserSods]);


    return (
        <React.Fragment>
            <TableRow onClick={() => setOpen(!open)} style={{backgroundColor: "#8ab6d6"}}>
                <TableCell>
                    <IconButton aria-label="expand row" size="medium" onClick={() => setOpen(!open)}>
                        {open ? <KeyboardArrowUpIcon/> : <KeyboardArrowDownIcon/>}
                    </IconButton>
                </TableCell>
                <TableCell component="td" scope="row">
                    {row.username}
                </TableCell>
                <TableCell>{row.email}</TableCell>
            </TableRow>


            <TableRow style={{textAlign: 'left'}}>
                <TableCell style={{padding: 0, margin: 0}} colSpan={4}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box margin={0}>

                            <Table size="medium">
                                <TableHead>
                                    <TableRow style={{backgroundColor: "#d8e3e7"}}>
                                        <TableCell> {t("_podId")}</TableCell>
                                        <TableCell>{t("_podName")}</TableCell>
                                        <TableCell>{t("_status")}</TableCell>
                                        <TableCell></TableCell>
                                    </TableRow>
                                </TableHead>


                                <TableBody>
                                    {userPods.map((podsRow) => (
                                        <TableRow key={podsRow.doc_id} hover role="checkbox"
                                                  onClick={() => handleNavigationPod(podsRow.doc_id)}>

                                            <TableCell component="th" scope="row">{podsRow.pod_id}</TableCell>
                                            <TableCell>{podsRow.name}</TableCell>

                                            <TableCell>

                                                {t("_" + podsRow.status)}

                                            </TableCell>

                                            <TableCell>
                                                {StatusIconManagement(podsRow.status)}
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>


                            </Table>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
            <TableRow style={{textAlign: 'left'}}>
                <TableCell style={{padding: 0, margin: 0, textAlign: 'left'}} colSpan={4}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box margin={0}>

                            <Table size="medium">
                                <TableHead>
                                    <TableRow style={{backgroundColor: "#d8e3e7"}}>
                                        <TableCell>{t("_sodId")}</TableCell>
                                        <TableCell>{t("_sodName")}</TableCell>
                                        <TableCell>{t("_status")}</TableCell>
                                        <TableCell></TableCell>
                                    </TableRow>
                                </TableHead>

                                <TableBody>
                                    {userSods.map((sodsRow) => (
                                        <TableRow key={sodsRow.doc_id} hover role="checkbox"
                                                  onClick={() => handleNavigationSod(sodsRow.doc_id)}>

                                            <TableCell component="th" scope="row">{sodsRow.sod_id}</TableCell>
                                            <TableCell>{sodsRow.name}</TableCell>

                                            <TableCell>


                                                {t("_" + sodsRow.status)}


                                            </TableCell>

                                            <TableCell>
                                                {StatusIconManagement(sodsRow.status)}
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>


                            </Table>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>


        </React.Fragment>
    );
}