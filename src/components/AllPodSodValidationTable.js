import React, {useEffect, useState} from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {usersStream} from "../database/firestore_requests";
import {mapDocToUser} from "../database/mapping_methods";
import {useTranslation} from "react-i18next";
import Row from "./PodSodRow";

//TABLE TO DISPLAY ALL PODS AND SODS, CALLING PodSodRow.js as Row inside the users
export default function AllSodPodValidationTable() {

    const [t] = useTranslation('common');
    let [users, setUsers] = useState([]);

    useEffect(() => {

        const unsubscribe = usersStream({
            next: querySnapshot => {
                const usersFirestore = querySnapshot.docs
                    .map((docSnapshot, index) => {
                        if (docSnapshot.data() !== undefined) {
                            return mapDocToUser(docSnapshot);
                        }
                    });

                usersFirestore.sort((a, b) => {
                    return a.username.localeCompare(b.username)
                });
                setUsers(usersFirestore);
            },
            error: (error) => {
                setUsers([]);
            }
        });
        return () => unsubscribe();
    }, [setUsers]);


    return (
        <TableContainer component={Paper}>
            <Table aria-label="collapsible table">
                <TableHead style={{backgroundColor: ' #5390d9 '}}>
                    <TableRow>
                        <TableCell/>
                        <TableCell>{t("_allBeginners")}</TableCell>
                        <TableCell>{t("_email")}</TableCell>
                    </TableRow>
                </TableHead>


                <TableBody>
                    {users.map((user) => (
                        <Row key={user.user_id} row={user}/>
                    ))}
                </TableBody>


            </Table>
        </TableContainer>

    );
}
