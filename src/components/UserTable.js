import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import {usersStream} from "../database/firestore_requests";
import {mapDocToUser} from "../database/mapping_methods";
import {useHistory} from "react-router-dom";
import "../App.css";
import {UserTableTitle} from "../utils/NameManagement";
import {useTranslation} from "react-i18next";

const useStyles = makeStyles({
    root: {
        width: '100%',
    },
    container: {
        height: '100%',
    },
});
const columns = [

    {id: 'user_id', label: "User id", minWidth: 170},
    {id: 'username', label: "User name", minWidth: 100},
    {
        id: 'role',
        label: "Role",
        minWidth: 170

    },
    {
        id: 'email',
        label: 'E-Mail',
        minWidth: 170,
        align: 'justify'
    },
];


//THIS TABLE DISPLAYS USERS
export default function UsersTable() {
    const [t] = useTranslation('common');
    const classes = useStyles();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    let [users, setUsers] = useState([]);
    let history = useHistory();

    //FILL THE USERS FROM DATABASE
    useEffect(() => {

        const unsubscribe = usersStream({
            next: querySnapshot => {
                const usersFirestore = querySnapshot.docs
                    .map((docSnapshot, index) => {
                        if (docSnapshot.data() !== undefined) {
                            return mapDocToUser(docSnapshot);
                        }
                    });

                usersFirestore.sort((a, b) => {
                    return a.username.localeCompare(b.username)
                });
                setUsers(usersFirestore);
            },
            error: (error) => {
                setUsers([]);
            }
        });
        return () => unsubscribe();
    }, [setUsers]);


    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };


    const handleNavigation = (id) => {

        history.push({
            pathname: `/user/${id}`
        })
    }


    return (


        <Paper className={classes.root}>
            <TableContainer className={classes.container}>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>


                        <TableRow>
                            {columns.map((column) => (
                                <TableCell
                                    key={column.id}
                                    align={column.align}
                                    style={{minWidth: column.minWidth, backgroundColor: "#8ab6d6"}}
                                >
                                    {UserTableTitle(column.label)}
                                </TableCell>
                            ))}
                        </TableRow>


                    </TableHead>


                    <TableBody>



                        {users.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((user) => {
                            return (
                                <TableRow hover role="checkbox" tabIndex={-1}
                                          onClick={() => handleNavigation(user.user_id)}
                                          key={user.user_id}>


                                    {columns.map((column) => {
                                        const value = user[column.id];


                                        return (

                                            <TableCell key={column.id} align={column.align}>


                                                {
                                                    column.label === "Role" ?

                                                        (() => {
                                                                switch (value) {
                                                                    case "Administrator":
                                                                        return t("_administrator")
                                                                    case "Beginner":
                                                                        return t("_beginner")
                                                                    case "Expert":
                                                                        return t("_expert")
                                                                    case "Advanced":
                                                                        return t("_advanced")
                                                                    default:
                                                                        return value
                                                                }
                                                            }

                                                        )()

                                                        :

                                                        value

                                                }

                                            </TableCell>

                                        );
                                    })}
                                </TableRow>


                            );
                        })}

                    </TableBody>


                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[10, 25, 100]}
                component="div"
                count={users.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={handleChangePage}
                labelRowsPerPage={t("_rowsPerPage")}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />
        </Paper>


    );
}
