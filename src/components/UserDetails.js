import React, {useEffect, useState} from "react";
import {
    AddExpert,
    Blacklist,
    Deletelist,
    getUser,
    ModifyAttributed,
    RemoveExpert,
    updateUser,
    usersStream,
    Whitelist
} from "../database/firestore_requests";
import {mapDocToUser} from "../database/mapping_methods";
import {
    Container,
    CssBaseline,
    FormControl,
    FormHelperText,
    InputLabel,
    makeStyles,
    MenuItem,
    Select,
    Typography
} from "@material-ui/core";
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import {useHistory} from "react-router-dom";
import {ArrowBack, Block, Delete, InsertEmoticon} from "@material-ui/icons";
import {green, orange, red} from "@material-ui/core/colors";
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import Paper from '@material-ui/core/Paper';
import {useTranslation} from "react-i18next";
import {RoleNameManagement} from "../utils/NameManagement";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


//USER DETAILS PAGE : MANAGE ROLE SELECTION, BLACK LIST, DELETION, BEGINNERS ATTRIBUTION


export default function UserDetails({match}) {

    const [t] = useTranslation('common');
    let {id} = match.params;
    let [user, setUser] = useState([]);
    const [left, setLeft] = useState([]);
    const [right, setRight] = useState([]);
    const [open, setOpen] = useState(false);
    const [btnDisabled, setBtnDisabled] = useState(false);
    const [isDeleted, setIsDeleted] = useState(false);
    let [expert_id, setExpert_id] = useState('NA');
    let [expertsArr, setExpertsArr] = useState([]);
    let [primaryArr, setPrimaryArr] = useState([]);

    useEffect(() => {
        // Fetch users, i.e. beginners, advanced and experts
        usersStream({
            next: querySnapshot => {
                const usersFirestore = querySnapshot.docs
                    .map((docSnapshot, index) => {
                        if (docSnapshot.data() !== undefined) {
                            return mapDocToUser(docSnapshot);
                        }
                    });
                const beginners = usersFirestore.filter(item => item.role === 'Beginner');
                const advanced_users = usersFirestore.filter(item => item.role === 'Advanced');
                const primary_users = beginners.concat(advanced_users);
                const primaryInfo = Object.fromEntries(
                    primary_users.map(({user_id, username, email, role}) => [user_id, {username, email, role}])
                );
                setPrimaryArr(primaryInfo);
                const beginnersNotAttr = usersFirestore.filter(item => item.role === 'Beginner' && (item.expert === undefined || item.expert === 'NA'));
                const advancedNotAttr = usersFirestore.filter(item => item.role === 'Advanced' && (item.expert === undefined || item.expert === 'NA'));
                const primaryNotAttr = beginnersNotAttr.concat(advancedNotAttr);
                setLeft(primaryNotAttr.map(item => item.user_id));
                let expertsAttr = usersFirestore.filter(item => item.role === 'Expert' && item.attributed_users !== undefined);
                setExpertsArr(expertsAttr);
            },
            error: (error) => {
                console.log("Test error message");
            }
        });
        // Fetch a user
        const unsubscribe = getUser({
            next: documentSnapshot => {
                let userFirestore = mapDocToUser(documentSnapshot);
                setUser(userFirestore);
                if (userFirestore.attributed_users === undefined) {
                    setRight([]);
                } else if (userFirestore.attributed_users.length === 0) {
                    setRight([]);
                } else {
                    setRight(userFirestore.attributed_users);
                }
                if (userFirestore.expert === undefined) {
                    setExpert_id('NA');
                } else if (userFirestore.expert === 'NA') {
                    setExpert_id('NA');
                } else {
                    setExpert_id(userFirestore.expert);
                }
                if (userFirestore.role === 'Administrator' || userFirestore.role === 'Deleted') {
                    setBtnDisabled(true);
                }
            },
            error: (error) => {
                setUser(undefined);
            }
        }, id);
        return () => unsubscribe();
    }, [setUser]);

    //Drop down list
    const useStyles = makeStyles((theme) => ({
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        },
        selectEmpty: {
            marginTop: theme.spacing(2),
        },
        button: {
            margin: theme.spacing(1),
        },
        root: {
            margin: 'auto',
        },
        paper: {
            width: 340,
            height: 230,
            overflow: 'auto',
        },
        button_transfer: {
            margin: theme.spacing(0.5, 0),
        },
    }));

    let [role, setRole] = React.useState(user.role);
    let history = useHistory();
    const classes = useStyles();

    const handleChangeRole = (event) => {
        setRole(event.target.value);
    };

    const handleClose = () => {
        setOpen(false);
    };

    // Function to modify a role
    async function handleModify() {
        const prev_role = user.role;
        let remove_flag = false;

        if (role === '' || role === undefined) {
            alert("You must select a role")
        } else {
            if (prev_role === 'Beginner' && (role === 'Expert' || role === 'Administrator')) {
                remove_flag = true;
            }
            if (prev_role === 'Advanced' && (role === 'Expert' || role === 'Administrator')) {
                remove_flag = true;
            }
            if (prev_role === 'Expert' && role !== 'Expert') {
                setOpen(true);
            } else {
                if (remove_flag) {
                    if (expert_id !== 'NA') {
                        await handleRemoveExpert();
                    }
                }
                handleUpdateUser();
            }
        }
    }

    // Function to delete expert user information or update expert to another role 
    async function handleUpdateExpertUser() {
        handleClose();
        right.map(item_id => removeExpertFromList(item_id));
        modifyAttributedOnExpertList([]);
        (isDeleted ? handleDeleteList() : handleUpdateUser());
    }

    // Function to update a role
    async function handleUpdateUser() {
        await updateUser(id, role);
        alert("Roles of " + user.username + " modified")
    }

    // Function to remove expert from beginner or advanced user and
    // to unattribute from expert attributed list
    async function handleRemoveExpert() {
        removeExpertFromList(id);
        let attributedListArr = expertsArr.filter(expert => expert.user_id === expert_id);
        let expertsArrRemoved = attributedListArr[0].attributed_users;
        let index = expertsArrRemoved.indexOf(id);
        if (index !== -1) {
            expertsArrRemoved.splice(index, 1);
            removeAttributedOnExpertList(expertsArrRemoved)
        }
    }

    async function handleBlacklist() {
        await Blacklist(id);
    }

    async function handleWhitelist() {
        await Whitelist(id);
    }

    // Function to delete a user
    async function handleDeleteUser() {
        const user_role = user.role;
        setIsDeleted(true);
        if (user_role === 'Expert') {
            setOpen(true);
        }
        if (user_role === 'Beginner' || user_role === 'Advanced') {
            if (expert_id !== 'NA') {
                handleRemoveExpert();
            }
            handleDeleteList();
        }
    }

    async function handleDeleteList() {
        setIsDeleted(false);
        await Deletelist(id);
    }

    // Function to remove expert from beginner or advanced user
    async function removeExpertFromList(user_id) {
        await RemoveExpert(user_id);
    }

    // Function to unattribute beginner or advanced user/s
    // from attributed list of a selected expert user
    async function modifyAttributedOnExpertList(id_array) {
        await ModifyAttributed(id, id_array);
    }

    async function addExpertToList(user_id) {
        await AddExpert(user_id, id);
    }

    // Function to unattribute beginner or advanced user
    // from attributed list of a specified expert
    async function removeAttributedOnExpertList(id_array) {
        await ModifyAttributed(expert_id, id_array);
    }

    // Transfer List
    function not(a, b) {
        return a.filter((value) => b.indexOf(value) === -1);
    }

    function intersection(a, b) {
        return a.filter((value) => b.indexOf(value) !== -1);
    }

    const [checked, setChecked] = useState([]);
    const leftChecked = intersection(checked, left);
    const rightChecked = intersection(checked, right);

    const handleToggle = (value) => () => {
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }

        setChecked(newChecked);
    };

    const handleAllRight = () => {
        left.map(item_id => addExpertToList(item_id));
        modifyAttributedOnExpertList(right.concat(left));
        setRight(right.concat(left));
        setLeft([]);
    };

    const handleCheckedRight = () => {
        leftChecked.map(item_id => addExpertToList(item_id));
        modifyAttributedOnExpertList(right.concat(leftChecked));
        setRight(right.concat(leftChecked));
        setLeft(not(left, leftChecked));
        setChecked(not(checked, leftChecked));
    };

    const handleCheckedLeft = () => {
        rightChecked.map(item_id => removeExpertFromList(item_id));
        const id_array = right;
        for (let i = 0; i < rightChecked.length; i++) {
            for (let j = 0; j < id_array.length; j++) {
                if (id_array[j] === rightChecked[i]) {
                    id_array.splice(j, 1);
                    j--;
                }
            }
        }
        modifyAttributedOnExpertList(id_array);
        setLeft(left.concat(rightChecked));
        setRight(not(right, rightChecked));
        setChecked(not(checked, rightChecked));
    };

    const handleAllLeft = () => {
        right.map(item_id => removeExpertFromList(item_id));
        modifyAttributedOnExpertList([]);
        setLeft(left.concat(right));
        setRight([]);
    };

    const customList = (items) => (
        <Paper className={classes.paper}>
            <List dense component="div" role="list">
                {items.map((value) => {
                    const labelId = `transfer-list-item-${value}-label`;
                    return (
                        <ListItem key={value} role="listitem" button onClick={handleToggle(value)}>
                            <ListItemIcon>
                                <Checkbox
                                    checked={checked.indexOf(value) !== -1}
                                    tabIndex={-1}
                                    disableRipple
                                    inputProps={{'aria-labelledby': labelId}}
                                />
                            </ListItemIcon>
                            <ListItemText id={labelId}
                                          primary={`${primaryArr[value].role} | ${primaryArr[value].username} | ${primaryArr[value].email}`}/>
                        </ListItem>
                    );
                })}
                <ListItem/>
            </List>
        </Paper>
    );

    return (
        <>
            <Button
                variant="contained"
                color="primary"
                className={classes.button}
                startIcon={<ArrowBack/>}
                onClick={() => history.goBack()}
            >
                {t("_back")}
            </Button>
            <CssBaseline/>
            <Container maxWidth="sm">
                <Typography component="div" style={{
                    backgroundColor: '#cfe8fc',
                    height: '100%',
                    width: '100%',
                    margin: '0px',
                    padding: '0px',
                    textAlign: 'center'
                }}>

                    {false ?
                        <h1>
                            This user was not found
                        </h1>

                        :
                        <>
                            <h2>
                                {user.username}
                            </h2>
                            <h3>
                                {user.email}
                            </h3>
                            <div>
                                <FormControl className={classes.formControl}>
                                    <InputLabel id="demo-simple-select-label"> {t("_role")}</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id='age-native-required'
                                        value={role}
                                        onChange={handleChangeRole}>
                                        <MenuItem value={"Beginner"}>{t('_beginner')}</MenuItem>
                                        <MenuItem value={"Advanced"}>{t('_advanced')}</MenuItem>
                                        <MenuItem value={"Expert"}>{t('_expert')}</MenuItem>
                                        <MenuItem value={"Administrator"}>{t('_administrator')}</MenuItem>
                                    </Select>
                                    <FormHelperText>{t('_currentRole')}: {RoleNameManagement(user.role)}</FormHelperText>
                                </FormControl>
                            </div>

                            <Button
                                variant="contained"
                                color="primary"
                                size="medium"
                                className={classes.button}
                                startIcon={<SaveIcon/>}
                                onClick={handleModify}
                            >
                                {t("_saveRole")}
                            </Button>
                            <Dialog
                                open={open}
                                onClose={handleClose}
                                aria-labelledby="alert-dialog-title"
                                aria-describedby="alert-dialog-description"
                            >
                                <DialogTitle
                                    id="alert-dialog-title">{isDeleted ? t("_dialogTitleDeleteExpert") : t("_dialogTitleChangeExpert")}</DialogTitle>
                                <DialogContent>
                                    <DialogContentText id="alert-dialog-description">{t("_dialogChangeExpert")}
                                    </DialogContentText>
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={handleClose} color="primary">
                                        {t("_cancel")}
                                    </Button>
                                    <Button onClick={handleUpdateExpertUser} color="primary" autoFocus>
                                        OK
                                    </Button>
                                </DialogActions>
                            </Dialog>
                            <br/>

                            {user.blacklist === false ?
                                <Button
                                    variant="contained"
                                    style={{backgroundColor: red[500]}}
                                    className={classes.button}
                                    startIcon={<Block/>}
                                    onClick={handleBlacklist}
                                >
                                    {t('_blockUser')}
                                </Button>
                                :
                                <Button
                                    variant="contained"
                                    style={{backgroundColor: green[500]}}
                                    className={classes.button}
                                    startIcon={<InsertEmoticon/>}
                                    onClick={handleWhitelist}
                                >
                                    {t('_unblockUser')}
                                </Button>
                            }
                            <br/>
                            <Button
                                variant="contained"
                                style={{backgroundColor: orange[500]}}
                                className={classes.button}
                                startIcon={<Delete/>}
                                disabled={btnDisabled}
                                onClick={handleDeleteUser}
                            >
                                {t('_deleteUser')}
                            </Button>
                        </>
                    }
                </Typography>
            </Container>
            <br/>
            <Container>
                <Typography component="div" style={{
                    height: '100%',
                    width: '100%',
                    margin: '0px',
                    padding: '0px',
                    textAlign: 'center'
                }}>
                    {(user.role === "Expert") ?
                        <Grid container spacing={2} justify="center" alignItems="center" className={classes.root}>
                            <Grid item>
                                <h3>{t('_notAttributedUser')}</h3>
                                {customList(left)}
                            </Grid>
                            <Grid item>
                                <Grid container direction="column" alignItems="center">
                                    <Button
                                        variant="outlined"
                                        size="small"
                                        className={classes.button_transfer}
                                        onClick={handleAllRight}
                                        disabled={left.length === 0}
                                        aria-label="move all right"
                                    >
                                        ≫
                                    </Button>
                                    <Button
                                        variant="outlined"
                                        size="small"
                                        className={classes.button_transfer}
                                        onClick={handleCheckedRight}
                                        disabled={leftChecked.length === 0}
                                        aria-label="move selected right"
                                    >
                                        &gt;
                                    </Button>
                                    <Button
                                        variant="outlined"
                                        size="small"
                                        className={classes.button_transfer}
                                        onClick={handleCheckedLeft}
                                        disabled={rightChecked.length === 0}
                                        aria-label="move selected left"
                                    >
                                        &lt;
                                    </Button>
                                    <Button
                                        variant="outlined"
                                        size="small"
                                        className={classes.button_transfer}
                                        onClick={handleAllLeft}
                                        disabled={right.length === 0}
                                        aria-label="move all left"
                                    >
                                        ≪
                                    </Button>
                                </Grid>
                            </Grid>
                            <Grid item>
                                <h3>{t('_attributedUser')}</h3>
                                {customList(right)}
                            </Grid>
                        </Grid>
                        :
                        <h1>
                        </h1>
                    }
                </Typography>
            </Container>
        </>
    );
}