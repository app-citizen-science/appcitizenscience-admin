import React, {useEffect, useMemo, useRef, useState} from "react";
import {deletePod, getPod, updatePod} from "../database/firestore_requests";
import {mapDocToPod} from "../database/mapping_methods";
import Button from "@material-ui/core/Button";
import {ArrowBack} from "@material-ui/icons";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import DeleteIcon from '@material-ui/icons/Delete';
import {
    FormControl,
    FormControlLabel,
    FormHelperText,
    FormLabel,
    Grid,
    InputLabel,
    makeStyles,
    MenuItem,
    Paper,
    RadioGroup,
    Select,
    Snackbar,
} from "@material-ui/core";
import {useHistory, useLocation} from "react-router-dom";
import {LanguageManagement, StatusManagement} from "../utils/NameManagement";
import SaveIcon from "@material-ui/icons/Save";
import Editable from "../editable/EditableTemplate";
import CreateIcon from '@material-ui/icons/Create';
import firebase from "firebase";
import {useTranslation} from "react-i18next";
import {deletePodImage} from "../database/storage_request";
import {MapContainer, Marker, Popup, TileLayer, useMap} from "react-leaflet";
import EditLocationIcon from "@material-ui/icons/EditLocation";
import {markerIconStart} from "../map/markerIcons";
import {BlueRadio, GreenRadio, GreyRadio, OrangeRadio, RedRadio} from "../utils/RadioCustom";


//DISPLAY CLICKED POD DETAIL


export default function PodDetails({match}) {

    const [t] = useTranslation('common');
    let {id} = match.params;
    const location = useLocation();
    let [pod, setPod] = useState([]);
    let [name, setName] = useState('');
    let [status, setStatus] = React.useState('');
    let [description, setDescription] = React.useState("");
    let [language, setLanguage] = React.useState("");
    let [technicalLevel, setTechnicalLevel] = React.useState(1);
    let [riskLevel, setRiskLevel] = React.useState(1);
    let [lat, setLat] = React.useState(0);
    let [long, setLong] = React.useState(0);
    const markerRef = useRef(null)
    const [draggable, setDraggable] = useState(true)
    const [open, setOpen] = React.useState(false);
    const [state, setState] = React.useState({open: false});


    useEffect(() => {

        const unsubscribe = getPod({
            next: documentSnapshot => {
                let podFirestore = mapDocToPod(documentSnapshot);
                setPod(podFirestore);

            },

            error: (error) => {
                setPod(undefined);
            }

        }, id);

        return () => unsubscribe();

    }, [setPod]);


    useEffect(() => {

        if (pod.geo_position !== undefined) {

            setLat(pod.geo_position._lat)
            setLong(pod.geo_position._long)
        }

    }, [pod.geo_position])


    useEffect(() => {

            if (pod !== undefined) {
                setTechnicalLevel(pod.level_technical)
                setRiskLevel(pod.level_risk)
                setStatus(pod.status)
                setName(pod.name)
                setDescription(pod.description)
                setLanguage(pod.language)

            }
        }, [pod]
    )


    function ChangeMapView({coords}) {
        const map = useMap();
        map.setView(coords, map.getZoom());

        return null;
    }

    const handleChangeLanguage = (event) => {
        setLanguage(event.target.value);

    };


    const draggablePodHandler = useMemo(
        () => ({
            dragend() {
                const marker = markerRef.current

                if (marker != null) {
                    setLat(marker.getLatLng().lat);
                    setLong(marker.getLatLng().lng);

                }
            },
        }),
        [],
    )


    const handleClickOpenDialog = () => {
        setOpen(true);
    };

    const handleCloseDialog = () => {
        setOpen(false);
    };


    async function handleSave() {


        await updatePod(id,
            status,
            description,
            name,
            new firebase.firestore.GeoPoint(lat, long),
            riskLevel,
            technicalLevel,
            language
        );


        //open the snackbar
        setState({open: true});

        //clear the local storage
        localStorage.clear();


    }


    const handleClose = () => {
        setState({
            ...state,
            open: false,
        });
    };


    //Styles
    const useStyles = makeStyles((theme) => ({
        root: {
            flexGrow: 1
        },
        paper: {
            padding: theme.spacing(1),
            margin: 15,
            textAlign: 'center',
            color: theme.palette.text.primary,
            borderRadius: "25px",
            backgroundPosition: "left top",
            backgroundRepeat: "repeat",
        },
        formControl: {
            margin: theme.spacing(1),
            width: '50%',
            height: '100%'
        },
        formTitle: {
            marginTop: 10,
            marginBottom: 10,
            fontSize: 18,
        },
        selectEmpty: {
            marginTop: theme.spacing(1),
        },
        button: {
            marginLeft: 10,
            marginTop: 10
        },
    }));


    let history = useHistory();
    const classes = useStyles();


    const handleChangeStatus = (event) => {
        setStatus(event.target.value);

    };


    const handleTechnical = (event) => {
        setTechnicalLevel(parseInt(event.target.value));
    };


    const handleRisk = (event) => {
        setRiskLevel(parseInt(event.target.value));
    };


    return (

        <>


            <div style={{padding: 5}}>


                <Button
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    startIcon={<ArrowBack/>}
                    onClick={() => history.goBack()}
                >
                    {t("_back")}
                </Button>

                {false ?

                    <header>
                        <h1>
                            {t("_whoops")}
                        </h1>
                    </header>
                    :


                    <div className={classes.root}>
                        <Grid container spacing={1}>
                            <Grid item xs={7}>
                                <Paper className={classes.paper}>

                                    <header>

                                        <h1>

                                            <Editable
                                                text={name}
                                                placeholder={t("_writeATitle")}
                                                type="input"
                                            >
                                                <input
                                                    type="text"
                                                    name="name"
                                                    placeholder="Name"
                                                    value={name}
                                                    onChange={e => setName(e.target.value)}
                                                />
                                            </Editable>

                                        </h1>
                                        <p><b> {t("_podId")} :</b> {pod.pod_id}</p>
                                        <p><b>{t("_owner")} :</b> {location.state.username}</p>


                                    </header>


                                </Paper>

                            </Grid>


                            <Grid item xs={5} style={{textAlign: 'justify'}}>


                                <CreateIcon color={"primary"}/>

                                <Editable
                                    text={description}
                                    placeholder={t("_writeADescription")}
                                    type="textarea"
                                >
                                        <textarea
                                            name="description"
                                            placeholder={t("_writeADescription")}
                                            value={description}
                                            onChange={e => setDescription(e.target.value)}
                                        />
                                </Editable>

                            </Grid>


                            <Grid item xs={4}>


                                <img src={pod.img_download_url} className="rcorners" style={{
                                    width: '100%',
                                    height: '400px',

                                }}/>


                            </Grid>
                            <Grid item xs={8}>


                                {true ?

                                    <div className="leaflet-container">

                                        <MapContainer center={[lat, long]} zoom={17} id="leafletMap">

                                            <TileLayer
                                                url="https://wmts.geo.admin.ch/1.0.0/ch.swisstopo.pixelkarte-grau/default/current/3857/{z}/{x}/{y}.jpeg"
                                            />
                                            <TileLayer
                                                url='https://wmts.geo.admin.ch/1.0.0/ch.swisstopo.swisstlm3d-wanderwege/default/current/3857/{z}/{x}/{y}.png'
                                            />

                                            <Marker
                                                zIndex={10}
                                                icon={markerIconStart}
                                                draggable={draggable}
                                                eventHandlers={draggablePodHandler}
                                                position={[lat, long]}
                                                ref={markerRef}>


                                                <Popup>

                                                    <h1>{pod.name}</h1>

                                                    <p>

                                                        {t("_moveTheMarker")}
                                                        <EditLocationIcon color={"primary"}/>
                                                    </p>


                                                </Popup>


                                            </Marker>

                                            <ChangeMapView coords={[lat, long]}/>
                                        </MapContainer>


                                    </div>
                                    :
                                    <div>
                                    </div>


                                }


                            </Grid>
                            <Grid item xs={4}>
                                <Paper className={classes.paper}>

                                    <FormLabel component="legend">{t("_technicalLevel")}</FormLabel>
                                    <RadioGroup row aria-label="position" style={{justifyContent: "center"}}>
                                        <FormControlLabel
                                            checked={technicalLevel === 1}
                                            onChange={handleTechnical}
                                            control={<GreenRadio/>}
                                            value={1}
                                            label="1"
                                            labelPlacement="bottom"
                                        />
                                        <FormControlLabel
                                            checked={technicalLevel === 2}
                                            onChange={handleTechnical}
                                            control={<BlueRadio/>}
                                            value={2}
                                            labelPlacement="bottom"
                                            label="2"
                                        />
                                        <FormControlLabel
                                            checked={technicalLevel === 3}
                                            onChange={handleTechnical}
                                            control={<OrangeRadio/>}
                                            value={3}
                                            labelPlacement="bottom"
                                            label="3"
                                        />
                                        <FormControlLabel
                                            checked={technicalLevel === 4}
                                            onChange={handleTechnical}
                                            control={<RedRadio/>}
                                            value={4}
                                            labelPlacement="bottom"
                                            label="4"
                                        />
                                        <FormControlLabel
                                            checked={technicalLevel === 5}
                                            onChange={handleTechnical}
                                            control={<GreyRadio/>}
                                            value={5}
                                            labelPlacement="bottom"
                                            label='5'
                                        />

                                    </RadioGroup>


                                </Paper>
                            </Grid>
                            <Grid item xs={4}>
                                <Paper className={classes.paper}>

                                    <FormLabel component="legend">{t("_riskLevel")}</FormLabel>
                                    <RadioGroup row aria-label="position" style={{justifyContent: "center"}}>
                                        <FormControlLabel
                                            checked={riskLevel === 1}
                                            onChange={handleRisk}
                                            control={<GreenRadio/>}
                                            value={1}
                                            label="1"
                                            labelPlacement="bottom"
                                        />
                                        <FormControlLabel
                                            checked={riskLevel === 2}
                                            onChange={handleRisk}
                                            control={<BlueRadio/>}
                                            value={2}
                                            label="2"
                                            labelPlacement="bottom"
                                        />
                                        <FormControlLabel
                                            checked={riskLevel === 3}
                                            onChange={handleRisk}
                                            control={<OrangeRadio/>}
                                            value={3}
                                            label="3"
                                            labelPlacement="bottom"
                                        />
                                        <FormControlLabel
                                            checked={riskLevel === 4}
                                            onChange={handleRisk}
                                            control={<RedRadio/>}
                                            value={4}
                                            label="4"
                                            labelPlacement="bottom"
                                        />
                                        <FormControlLabel
                                            checked={riskLevel === 5}
                                            onChange={handleRisk}
                                            control={<GreyRadio/>}
                                            value={5}
                                            label="5"
                                            labelPlacement="bottom"
                                        />

                                    </RadioGroup>


                                </Paper>
                            </Grid>


                            <Grid item md={3} className={classes.paper} style={{textAlign: 'left'}}>


                                <Grid>
                                    <FormControl className={classes.formControl}>
                                        <InputLabel>{t("_status")}</InputLabel>
                                        <Select
                                            defaultValue={status}
                                            onChange={handleChangeStatus}>

                                            <MenuItem value={"validated"}>{t("_validated")}</MenuItem>
                                            <MenuItem value={"rejected"}>{t("_rejected")}</MenuItem>
                                            <MenuItem value={"toChange"}>{t("_toChange")}</MenuItem>
                                            <MenuItem value={"toValidate"}>{t("_toValidate")}</MenuItem>
                                        </Select>
                                        <FormHelperText>{t("_currentStatus")} : {' '}

                                            {StatusManagement(pod.status)}

                                        </FormHelperText>
                                    </FormControl>
                                </Grid>


                                <Grid>

                                    <FormControl className={classes.formControl}>
                                        <InputLabel>{t("_language")}</InputLabel>
                                        <Select
                                            defaultValue={language}
                                            onChange={handleChangeLanguage}>

                                            <MenuItem value={"en"}>{t("_english")}</MenuItem>
                                            <MenuItem value={"fr"}>{t("_french")}</MenuItem>
                                            <MenuItem value={"de"}>{t("_german")}</MenuItem>
                                            <MenuItem value={"it"}>{t("_italian")}</MenuItem>

                                        </Select>
                                        <FormHelperText>{t("_currentLanguage")} : {' '}

                                            {LanguageManagement(pod.language)}

                                        </FormHelperText>
                                    </FormControl>


                                </Grid>


                            </Grid>


                            <Grid item xs={6}>

                                <div style={{textAlign: 'center'}}>

                                    <Button
                                        variant="contained"
                                        color="secondary"
                                        size="medium"
                                        className={classes.button}
                                        startIcon={<DeleteIcon/>}
                                        onClick={handleClickOpenDialog}
                                    >
                                        {t("_deletePod")}
                                    </Button>
                                    <Dialog
                                        open={open}
                                        onClose={handleCloseDialog}
                                        aria-labelledby="alert-dialog-title"
                                        aria-describedby="alert-dialog-description"
                                    >
                                        <DialogTitle id="alert-dialog-title"> {t("_warning")}</DialogTitle>
                                        <DialogContent>
                                            <DialogContentText id="alert-dialog-description">
                                                {t("_deletePodVerification")}
                                            </DialogContentText>
                                        </DialogContent>
                                        <DialogActions>
                                            <Button onClick={handleCloseDialog} color="primary">
                                                {t("_cancel")}
                                            </Button>
                                            <Button color="secondary" autoFocus
                                                    onClick={

                                                        async () => {
                                                            await history.push("/pod-sod-validation");
                                                            await deletePodImage(pod, pod.doc_id).then(() => {
                                                                    deletePod(pod.doc_id);
                                                                }
                                                            );
                                                        }
                                                    }
                                            >
                                                {t("_yesDelete")}
                                            </Button>
                                        </DialogActions>
                                    </Dialog>

                                </div>


                            </Grid>


                            <Grid item xs={6}>

                                <div style={{textAlign: 'center'}}>
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        size="medium"
                                        className={classes.button}
                                        startIcon={<SaveIcon/>}
                                        onClick={handleSave}
                                    >
                                        {t("_saveModification")}
                                    </Button>


                                </div>


                            </Grid>


                        </Grid>

                    </div>


                }

                <br/>


            </div>


            <Snackbar
                open={state.open}
                onClose={handleClose}
                message={t("_podModified")}
            />


        </>


    );


}
