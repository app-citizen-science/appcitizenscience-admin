

// set the different attribute to the user, related to firebase document /users
export const mapDocToUser= (document) => {
    return {
        user_id: document.id,
        role: document.data().role,
        username: document.data().username,
        email: document.data().email,
        blacklist: document.data().blacklist,
        expert: document.data().expert,
        attributed_users: document.data().attributed_users

    }
}

// set the different attribute to the user, related to firebase document /pointsofdifficulty
export const mapDocToPod = (document) => {
    return {
        pod_id: document.data().pod_id,
        doc_id: document.id,
        fk_sod_id: document.data().fk_sod_id,
        fk_user_id: document.data().fk_user_id,
        date: document.data().date,
        description: document.data().description,
        geo_position: document.data().geo_position,
        img_download_url: document.data().img_download_url,
        level_risk: document.data().level_risk,
        level_technical: document.data().level_technical,
        name: document.data().name,
        status: document.data().status,
        language: document.data().language,
    }
}

// set the different attribute to the user, related to firebase document /segmentsofdifficulty
export const mapDocToSod = (document) => {
    return {
        sod_id: document.data().sod_id,
        doc_id: document.id,
        fk_user_id: document.data().fk_user_id,
        date: document.data().date,
        description: document.data().description,
        geo_position_start: document.data().geo_position_start,
        geo_position_end: document.data().geo_position_end,
        img_download_url: document.data().img_download_url,
        level_risk: document.data().level_risk,
        level_technical: document.data().level_technical,
        name: document.data().name,
        status: document.data().status,
        modification_date: document.data().modification_date,
        language: document.data().language,
    }
}