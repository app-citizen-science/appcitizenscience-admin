import firebase from "firebase";


// FUNCTION TO DELETE THE POD IMAGE IN FIREBASE STORAGE
export async function deletePodImage(pod, doc_id) {

    const fileExtension = pod.img_download_url.toString().substring(pod.img_download_url.indexOf('?') - 3, pod.img_download_url.indexOf('?'));

    const ref = await firebase
        .storage()
        .ref()
        .child('pods/' + doc_id + '.' + fileExtension);

    ref.delete();


}


// FUNCTION TO DELETE THE SOD IMAGE IN FIREBASE STORAGE
export async function deleteSodImage(pod, doc_id) {
    const fileExtension = pod.img_download_url.toString().substring(pod.img_download_url.indexOf('?') - 3, pod.img_download_url.indexOf('?'));

    const ref = await firebase
        .storage()
        .ref()
        .child('sods/' + doc_id + '.' + fileExtension);

    ref.delete();
}
