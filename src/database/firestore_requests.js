import 'firebase/firestore';
import {db} from "../config/firebase";
import firebase from "firebase";

//All users
export function usersStream(observer){
    return db.collection('users')
        .onSnapshot(observer);
}

//All pods belonging to a specific user
export function myUserPodsStream(observer, userId){
    return db.collection('pointsOfDifficulty')
        .where('fk_user_id', '==', userId)
        .onSnapshot(observer);
}

// FUNCTION TO GET A SPECIFIC POD
export function getPod(observer, podId) {
    return db.collection('pointsOfDifficulty')
        .doc(podId)
        .onSnapshot(observer);
}

//Get all pods from an expert
export function usersFromExpert(observer, currentUserId){
    return db.collection('users')
        .where('expert', '==', currentUserId)
        .onSnapshot(observer);
}

// get a user from an id
export function getUser(observer, user_id) {
    return db.collection('users')
        .doc(user_id)
        .onSnapshot(observer);
}

// FUNCTION TO UPDATE USER DETAILS
export function updateUser(doc_id, role){

    return db.collection("users").doc(doc_id).update({
        'role': role,
    }).then(() => {
        console.log('User updated');
    });
}

// FUNCTION TO UPDATE POD DETAILS
export function updatePod(doc_id, status, description, name, geo_position, level_risk, level_technical, language){

    return db.collection("pointsOfDifficulty").doc(doc_id).update({
        'status': status,
        'description': description,
        'name': name,
        'geo_position': geo_position,
        'level_risk': level_risk,
        'level_technical': level_technical,
        'language': language,
        'modification_date': new firebase.firestore.FieldValue.serverTimestamp()

    }).then(() => {
        console.log('Pod updated');
    });
}

// FUNCTION TO DELETE A POD
export function deletePod(doc_id) {
    db.collection("pointsOfDifficulty").doc(doc_id).delete().then(() => {
        console.log("The pod has been deleted successfully.");
    });
}

// FUNCTION TO UPDATE POD DETAILS WITHOUT THE MAP
export function updatePodNoGeo(doc_id, status, description, name, level_risk, level_technical){

    return db.collection("pointsOfDifficulty").doc(doc_id).update({
        'status': status,
        'description': description,
        'name': name,
        'level_risk': level_risk,
        'level_technical': level_technical
    }).then(() => {
        console.log('Pod updated');
    });
}

// FUNCTION TO BLACKLIST
export function Blacklist(doc_id){

    return db.collection("users").doc(doc_id).update({
        'blacklist': true,
    }).then(() => {
        console.log('User updated');
    });
}

// FUNCTION TO WHITELIST
export function Whitelist(doc_id){

    return db.collection("users").doc(doc_id).update({
        'blacklist': false,
    }).then(() => {
        console.log('User updated');
    });
}

// FUNCTION TO DELETE LIST
export function Deletelist(doc_id){
    return db.collection("users").doc(doc_id).update({
        'username': 'Deleted',        
        'email': 'Deleted',
        'role': 'Deleted',        
    }).then(() => {
        console.log('User deleted');
    });
}

// FUNCTION TO REMOVE EXPERT FROM BEGINNER OR ADVANCED USER IN FIREBASE
export function RemoveExpert(doc_id){
    return db.collection("users").doc(doc_id).update({
        'expert': "NA",
    }).then(() => {
        console.log('Beginner or Advances user list updated');
    });
}

// FUNCTION TO ADD EXPERT TO BEGINNER OR ADVANCED USER IN FIREBASE
export function AddExpert(doc_id, expert_id){
    return db.collection("users").doc(doc_id).update({
        'expert': expert_id,
    }).then(() => {
        console.log('Begginer or Advanced user list updated');
    });
}

// FUNCTION TO MODIFY ATTRIBUTED USER ARRAY FOR EXPERT IN FIREBASE
export function ModifyAttributed(doc_id, id_array){
    return db.collection("users").doc(doc_id).update({
        'attributed_users': id_array,
    }).then(() => {
        console.log('Expert user list updated');
    });
}

//--------------------------------
//SOD
//--------------------------------
// FUNCTION TO GET THE SOD OF A USER IN FIREBASE
export function myUserSodsStream(observer, userId) {
    return db.collection('segmentsOfDifficulty')
        .where('fk_user_id', '==', userId)
        .onSnapshot(observer);
}

// FUNCTION TO GET A SPECIFIC SOD
export function getSod(observer, sodId) {
    return db.collection('segmentsOfDifficulty')
        .doc(sodId)
        .onSnapshot(observer);
}

// FUNCTION TO UPDATE SOD DETAILS
export function updateSod(doc_id, description, name, riskLevel, technicalLevel, geoPointStart, geoPointEnd, status, language) {
    return db.collection("segmentsOfDifficulty").doc(doc_id).update({
        'description': description,
        'level_risk': riskLevel,
        'level_technical': technicalLevel,
        'name': name,
        'geo_position_start': geoPointStart,
        'geo_position_end': geoPointEnd,
        'status': status,
        'language': language,
        'modification_date': new firebase.firestore.FieldValue.serverTimestamp()

    }).then(() => {
        console.log('SOD updated !');
    });
}

// FUNCTION TO DELETE A SOD
export function deleteSod(doc_id) {
    db.collection("segmentsOfDifficulty").doc(doc_id).delete().then(() => {
        console.log("The sod has been deleted successfully.");
    });
}