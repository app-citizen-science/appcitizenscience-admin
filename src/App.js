import React, {useEffect, useState} from 'react';
import "./App.css";
import {BrowserRouter, Route} from "react-router-dom";
import {AuthProvider} from "./navigation/Authentication";
import {PrivateRoute} from "./navigation/PrivateRoute";
import {NavScreen} from './navigation/NavScreen';
import SignIn from './screens/SignIn'
import UserDetails from "./components/UserDetails";
import PodDetails from "./components/PodDetails";
import SodDetails from "./components/SodDetails";
import {UserContext} from "./UserContext";
import firebase from "firebase";
import {getUser} from "./database/firestore_requests";


//ROUTES AND USER CONTEXT ARE SET HERE

function App() {


    const [userAuthenticated, setUserAuthenticated] = useState();
    const [user, setUser] = useState();

    useEffect(() => {
            const subscriber = firebase.auth().onAuthStateChanged(userAuth => {
                if (userAuth != null) {
                    setUserAuthenticated(userAuth);
                } else {
                    setUserAuthenticated(undefined);
                }
            });
            return () => subscriber();
        }, []
    );

    useEffect(() => {
        //Method to get the infos of a user
        if (userAuthenticated !== undefined) {
            const unsubscribe = getUser({
                next: querySnapshot => {
                    const userFirestore = querySnapshot.data();

                    setUser(userFirestore);
                },
                error: (error) => {
                    setUser(null);
                }
            }, userAuthenticated.uid);
            return () => unsubscribe();
        }
    }, [userAuthenticated]);

    return (

        <UserContext.Provider value={{
            userAuthenticated: userAuthenticated,
            user: user,
            setUserAuthenticated: setUserAuthenticated,
            setUser: setUser,
        }}>
            <AuthProvider>
                <BrowserRouter>
                    <div>
                        <PrivateRoute exact path="/" component={NavScreen}/>
                        <PrivateRoute exact path="/user-management" component={NavScreen}/>
                        <PrivateRoute exact path="/pod-sod-validation" component={NavScreen}/>
                        <PrivateRoute exact path="/all-pod-sod-validation" component={NavScreen}/>
                        <PrivateRoute exact path="/contact" component={NavScreen}/>
                        <Route exact path="/signin" component={SignIn}/>
                        <PrivateRoute path="/user/:id" component={UserDetails}/>
                        <PrivateRoute path="/pod/:id" component={PodDetails}/>
                        <PrivateRoute path="/sod/:id" component={SodDetails}/>

                    </div>
                </BrowserRouter>
            </AuthProvider>
        </UserContext.Provider>
    );
}

export default App;